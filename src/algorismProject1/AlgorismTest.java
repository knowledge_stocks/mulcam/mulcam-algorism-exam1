package algorismProject1;

import java.util.Scanner;

public class AlgorismTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int studentCount = 0;
		while(true) {
			try {
				System.out.print("학생 수 입력: ");
				studentCount = Integer.parseInt(scanner.nextLine());
				if(studentCount > 0) {
					break;
				} else {
					throw new NumberFormatException();
				}
			} catch (NumberFormatException e) {
				System.out.println("학생 수는 0보다 큰 정수로 입력해주세요.");
			}
		}
		
		int[] scores = new int[studentCount];
		for(int i = 0; i < scores.length; i++) {
			int score;
			while(true) {
				try {
					System.out.printf("학생 %d 점수 입력: ", i + 1);
					score = Integer.parseInt(scanner.nextLine());
					break;
				} catch (NumberFormatException e) {
					System.out.println("점수는 정수 형태로 입력해주세요.");
				}
			}
			scores[i] = score;
		}
		scanner.close();
		
		quickSortDesc(scores, 0, scores.length - 1);
		
		for (int i = 0; i < scores.length; i++) {
			System.out.printf("%d등: %d\n", i + 1, scores[i]);
		}
	}

	public static void quickSortAsc(int[] arr, int left, int right) {
		int lPointer = left;
		int rPointer = right;
		int pivot = arr[(lPointer + rPointer) / 2];
		
		while(lPointer <= rPointer) {
			while(pivot < arr[rPointer]) {
				rPointer--;
			}
			while(pivot > arr[lPointer]) {
				lPointer++;
			}
			if(lPointer <= rPointer) {
				int tmp = arr[lPointer];
				arr[lPointer++] = arr[rPointer];
				arr[rPointer--] = tmp;
			}
		}
		
		if(left < rPointer) {
			quickSortAsc(arr, left, rPointer);
		}
		if(right > lPointer) {
			quickSortAsc(arr, lPointer, right);
		}
	}
	
	public static void quickSortDesc(int[] arr, int left, int right) {
		int lPointer = left;
		int rPointer = right;
		int pivot = arr[(lPointer + rPointer) / 2];
		
		while(lPointer <= rPointer) {
			while(pivot > arr[rPointer]) {
				rPointer--;
			}
			while(pivot < arr[lPointer]) {
				lPointer++;
			}
			if(lPointer <= rPointer) {
				int tmp = arr[lPointer];
				arr[lPointer++] = arr[rPointer];
				arr[rPointer--] = tmp;
			}
		}
		
		if(left < rPointer) {
			quickSortDesc(arr, left, rPointer);
		}
		if(right > lPointer) {
			quickSortDesc(arr, lPointer, right);
		}
	}
}
